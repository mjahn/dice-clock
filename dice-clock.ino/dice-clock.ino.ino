#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

#include <Wire.h>
#include <TM1637Display.h>
#include <Pushbutton.h>

#include <EthernetUdp.h>

#define DCF_PIN 3                // Connection pin to DCF 77 device
#define DCF_INTERRUPT 0          // Interrupt number associated with pin
#define DS1307_I2C_ADDRESS 0x68 // I2C Adresse des RTC  DS3231
#define DIO D0
#define CLK D5
#define localPort 8888      // local port to listen for UDP packets. needed for the NTP servers for time updating
#define NTP_PACKET_SIZE 48 // NTP time stamp is in the first 48 bytes of the message
 
Pushbutton button_mode(D6);
Pushbutton button_plus(D7);
Pushbutton button_minus(D8);

// Non-inverted input on pin DCF_PIN
TM1637Display display(CLK, DIO);
WiFiManager wifiManager;
EthernetUDP Udp;// A UDP instance to let us send and receive packets over UDP

byte packetBuffer[ NTP_PACKET_SIZE ]; //buffer to hold incoming and outgoing packets
bool colonVisible = false;      
int pause = 500;
 
int stunde,minute,sekunde;
   
void setup(){
  wifiManager.autoConnect("DiceClock");
  Serial.begin(9600);
  display.setBrightness(0x0a); 
}
 
void loop(){
  if (button_mode.getSingleDebouncedPress())
  {
    // The button was pressed, so perform some action.
  }

  if (button_plus.getSingleDebouncedPress())
  {
    // The button was pressed, so perform some action.
  }

  if (button_minus.getSingleDebouncedPress())
  {
    // The button was pressed, so perform some action.
  }

   if(colonVisible){
     Serial.print(stunde);
     Serial.print(":");
     Serial.print(minute);
     Serial.print(":");
     Serial.println(sekunde);
        
     display.showNumberDecEx(1000 + stunde * 100 + minute, 64, true);
   } else {
     display.showNumberDecEx(1000 + stunde * 100 + minute, 0, true);
   }
   colonVisible = !colonVisible;
   delay(pause);
}

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return ( (val/10*16) + (val%10) );
}

// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return ( (val/16*10) + (val%16) );
}

// 1) Sets the date and time on the ds1307
// 2) Starts the clock
// 3) Sets hour mode to 24 hour clock

// Assumes you're passing in valid numbers

void setDateDs1307(byte set_second,        // 0-59
byte set_minute,        // 0-59
byte set_hour,          // 1-23
byte set_dayOfWeek,     // 1-7
byte set_dayOfMonth,    // 1-28/29/30/31
byte set_month,         // 1-12
byte set_year)          // 0-99
{
  Wire.beginTransmission(DS1307_I2C_ADDRESS);
  Wire.write(0);
  Wire.write(decToBcd(set_second));    // 0 to bit 7 starts the clock
  Wire.write(decToBcd(set_minute));
  Wire.write(decToBcd(set_hour));     
  Wire.write(decToBcd(set_dayOfWeek));
  Wire.write(decToBcd(set_dayOfMonth));
  Wire.write(decToBcd(set_month));
  Wire.write(decToBcd(set_year));
  Wire.write(0x10); // sends 0x10 (hex) 00010000 (binary) to control register - turns on square wave
  Wire.endTransmission();
}

// Gets the date and time from the ds1307
void getDateDs1307(byte *get_second,
byte *get_minute,
byte *get_hour,
byte *get_dayOfWeek,
byte *get_dayOfMonth,
byte *get_month,
byte *get_year)
{
  // Reset the register pointer
  Wire.beginTransmission(DS1307_I2C_ADDRESS);
  Wire.write(0);
  Wire.endTransmission();

  Wire.requestFrom(DS1307_I2C_ADDRESS, 7);

  // A few of these need masks because certain bits are control bits
  *get_second     = bcdToDec(Wire.read() & 0x7f);
  *get_minute     = bcdToDec(Wire.read());
  *get_hour       = bcdToDec(Wire.read() & 0x3f);  // Need to change this if 12 hour am/pm
  *get_dayOfWeek  = bcdToDec(Wire.read());
  *get_dayOfMonth = bcdToDec(Wire.read());
  *get_month      = bcdToDec(Wire.read());
  *get_year       = bcdToDec(Wire.read());
}

void GET_NTP_TIME(int timezone)
{
  sendNTPpacket(); // send an NTP packet to a time server
  //wdt_reset();
    // wait to see if a reply is available
  delay(1000);  
  wdt_reset();
  if ( Udp.parsePacket() ) {  
    // We've received a packet, read the data from it
    Udp.read(packetBuffer,NTP_PACKET_SIZE);  // read the packet into the buffer

    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);  
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;  

    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;     
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;                                
    

    // print the hour, minute and second:    
    SETHOUR = int(((epoch+(timezone*3600))  % 86400)/3600);      
    SETMINUTE = int((epoch  % 3600)/60); 
    SETSECONDS = int(epoch %60);
  }
}

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(void)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:            
  Udp.beginPacket(ip, 123); //NTP requests are to port 123
  Udp.write(packetBuffer,NTP_PACKET_SIZE);
  Udp.endPacket();
}
